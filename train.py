__author__ = "tuanhleo101"

import argparse
import sys

from libs.dataset.dataset_factory import load_data
from libs.modeling.model_builder import create_model
from libs.utils.config_parse import cfg
from libs.utils.config_parse import cfg_from_file


def parse_args():
    parser = argparse.ArgumentParser(description="Single Shot Detection")
    parser.add_argument('--cfg', dest='config_file', help='optional config file',
                        default=None, type=str)
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args


def train():
    # args = parse_args()
    # if args.config_file is not None:
    #     cfg_from_file(args.config_file)
    # cfg_from_file("cfgs/fssd_vgg16_train_voc.yml")
    cfg_from_file("cfgs/fssd_vgg16_train_smartshop.yml")
    # preparing()
    # train_model()


if __name__ == '__main__':
    train()
    train_loader = load_data(cfg, 'train') if 'train' in cfg.PHASE else None
    model, priorbox = create_model(cfg.MODEL)
    print(priorbox.forward().size())
    #
    # with torch.no_grad():
    #     priors = Variable(priorbox.forward())
    #
    # detector = Detect(cfg.POST_PROCESS, priors)
    #
    # # print(priors.size())
    #
    # criterion = MultiBoxLoss(cfg.MATCHER, priors, use_gpu=False)
    # batch = iter(train_loader)
    # imgs, targets = next(batch)
    # out = model(imgs, 'train')
    # print(criterion.repulsionloss_without_RepBox(out, targets))
    # out = (out[0], model.softmax(out[1].view(-1, model.num_classes)))
    # print(out[0].size(), out[1].size(), priors.size())

    # detect
    # detections = detector.forward(out)
    # print(detections)
