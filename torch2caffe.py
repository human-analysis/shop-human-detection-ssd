import os
import random
import time
from glob import glob

import caffe2.python.onnx.backend
import cv2
# Prepare the inputs, here we use numpy to generate some random inputs for demo purpose
import numpy as np
import onnx
from caffe2.python.onnx.backend import Caffe2Backend as c2
from onnx import ModelProto


def model_prepation(onnx_path):
    with open(onnx_path, 'rb') as onnx_model:
        onnx_model_proto = ModelProto()
        onnx_model_proto.ParseFromString(onnx_model.read())
        init_net, predict_net = c2.onnx_graph_to_caffe2_net(onnx_model_proto)

        with open("darknet19" + "_init.pb", "wb") as f:
            f.write(init_net.SerializeToString())
        with open("darknet19" + "_predict.pb", "wb") as f:
            f.write(predict_net.SerializeToString())


def preproc_for_test(image, insize=[300, 300], mean=(103.94, 116.78, 123.68)):
    interp_methods = [cv2.INTER_LINEAR, cv2.INTER_CUBIC, cv2.INTER_AREA, cv2.INTER_NEAREST, cv2.INTER_LANCZOS4]
    interp_method = interp_methods[random.randrange(5)]
    image = cv2.resize(image, (insize[0], insize[1]), interpolation=interp_method)
    image = image.astype(np.float32)
    image -= mean
    return image.transpose(2, 0, 1)


if __name__ == "__main__":
    # Load the ONNX model
    model = onnx.load("basic.onnx")
    # model_prepation("optimized_graph.onnx")
    image_samples = glob(os.path.join('./sample/images', '*.jpg'))
    rep = caffe2.python.onnx.backend.prepare(model)
    rep.predict_net.type = 'prof_dacg'
    #
    # # Run the ONNX model with Caffe2
    total_time = 0
    img = np.random.randint(0, 40, (1, 3, 300, 300))
    for path in image_samples:
        # img = cv2.imread(path)
        start = time.time()
        # img = preproc_for_test(img)
        # img = img[np.newaxis, :, :, :]
        # print(img.shape)
        outputs = rep.run(img)
        total_time += (time.time() - start)
    #
    print(total_time / 100)
