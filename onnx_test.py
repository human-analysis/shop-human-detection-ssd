import os
import random
import time

import cv2
import numpy as np
import onnx
from onnx import optimizer
from onnx_tf.backend_rep import *

from libs.utils.config_parse import cfg


class Preproc():
    def __init__(self, resize, rgb_means):
        self.means = rgb_means
        self.resize = resize

    def __call__(self, image):
        insize = self.resize
        mean = self.means
        interp_methods = [cv2.INTER_LINEAR, cv2.INTER_CUBIC, cv2.INTER_AREA, cv2.INTER_NEAREST, cv2.INTER_LANCZOS4]
        interp_method = interp_methods[random.randrange(5)]
        image = cv2.resize(image, (insize[0], insize[1]), interpolation=interp_method)
        image = image.astype(np.float32)
        image -= mean
        image = image.transpose(2, 0, 1)
        return image


def ONXX_optimizing(model_path):
    original_model = onnx.load(model_path)
    print('The model before optimization:\n{}'.format(original_model))

    # A full list of supported optimization passes can be found using get_available_passes()
    all_passes = optimizer.get_available_passes()
    print("Available optimization passes:")
    for p in all_passes:
        print(p)
    print()

    # Pick one pass as example
    passes = ['fuse_consecutive_transposes']

    # Apply the optimization on the original model
    optimized_model = optimizer.optimize(original_model, passes)

    print('The model after optimization:\n{}'.format(optimized_model))

    # One can also apply the default passes on the (serialized) model
    # Check the default passes here: https://github.com/onnx/onnx/blob/master/onnx/optimizer.py#L43
    optimized_model = optimizer.optimize(original_model)
    onnx.save(optimized_model, "optimized_" + model_path)


if __name__ == "__main__":
    # # onnx to caffe2
    # "=================================="
    # cfg_from_file('./cfgs/fssd_darknet19_voc.yml')
    # model, priorbox = create_model(cfg.MODEL)
    # with torch.no_grad():
    #     priors = Variable(priorbox.forward())
    # use_gpu = torch.cuda.is_available()
    #
    # checkpoint = torch.load(cfg.RESUME_CHECKPOINT, map_location='gpu' if use_gpu else 'cpu')
    # model.load_state_dict(checkpoint)
    # model.train(False)
    #
    # dummy_input = torch.randn(1, 3, cfg.MODEL.IMAGE_SIZE[0], cfg.MODEL.IMAGE_SIZE[1])
    # torch_out = torch.onnx._export(model, dummy_input, "graph.onnx", export_params=True)

    # # plot graph to png
    # output_dir = './storage/caffe_dir/'
    # # plot_graph(output_var, os.path.join(output_dir, 'darknet19_voc.dot'))
    # # pytorch2caffe(input_var, output_var,
    # #               os.path.join(output_dir, 'darknet-pytorch2caffe.prototxt'),
    # #               os.path.join(output_dir, 'darknet-pytorch2caffe.caffemodel'))
    # onnx_model = onnx.load("graph.onnx")
    # predictor = None
    # with open("graph.onnx", 'rb') as onnx_model:
    #     onnx_model_proto = ModelProto()
    #     onnx_model_proto.ParseFromString(onnx_model.read())
    #     init_net, predict_net = c2.onnx_graph_to_caffe2_net(onnx_model_proto)
    #     predictor = workspace.Predictor(init_net, predict_net)
    # # onnx.checker.check_model(onnx_model)
    path = os.path.join('./sample/images', '190509_LLQ201_01_00382.jpg')
    # for path in images_list:
    image = cv2.imread(path)
    scale = [image.shape[1], image.shape[0], image.shape[1], image.shape[0]]
    preproc = Preproc(cfg.MODEL.IMAGE_SIZE, cfg.DATASET.PIXEL_MEANS)
    #
    start = time.time()
    image = preproc(image)
    image = np.expand_dims(image, 0)
    # out = predictor.run({"0": image})
    # print(time.time() - start, '=========================')

    # "================================="
    # init_net, predict_net = Caffe2Backend.onnx_graph_to_caffe2_net(onnx_model.graph)
    # # with open(output_dir + "darknet19" + "_init.pb", "wb") as f:
    # #     f.write(init_net.SerializeToString())
    # # with open(output_dir + "darknet19" +  "_predict.pb", "wb") as f:
    # #     f.write(predict_net.SerializeToString())
    # ===============================
    # img = Image.open("./sample/images/test.jpeg")
    # img = img.resize((300, 300), Image.ANTIALIAS)
    #
    # predictor = workspace.Predictor(init_net, predict_net)

    # img_out = workspace.FetchBlob("Insert number that was printed above")
    # /=====================================/
    # import onnxruntime
    # session = onnxruntime.InferenceSession("graph.onnx")
    # session.get_modelmeta()
    # first_input_name = session.get_inputs()[0].name
    # first_output_name = session.get_outputs()[0].name
    # second_output_name = session.get_outputs()[1].name
    # results = session.run(["372"], {"0": image})
    # print(time.time() - start)

    net = cv2.dnn.readNetFromONNX("./test.onnx")
    # ONXX_optimizing("mobilenetv1_fssd.onnx")
    # model = onnx.load("graph.onnx")
    # tf_rep = prepare(model, strict=False)
    # start = time.time()
    # tf_rep.run(image)
    # print(time.time() - start, '=====================')

# interpolation
