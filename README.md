# SSD 
Repository for SSD and its variants, implemented with python3

Currently, it contains these features:
- **Multiple SSD Variants**: fssd, fssd-lite.In addition, i added yolo implementation 
- **Multiplt BackBone**: Mobilenetv1, Mobilenetv2, vgg, darknet19, darknet53
- **Auxiliary loss**: Repulsion loss
- **Free Image Size**
- **Visualization** with [tensorboard-pytorch]

### Table of Contents
- <a href='#installation'>Installation</a>
- <a href='#dataset'>Dataset </a>
- <a href='#usage'>Usage</a>
- <a href='#performance'>Performance and Model</a>
- <a href='#visualization'>Visualization</a>
- <a href='#todo'>Future Work</a>
- <a href='#reference'>Reference</a>

## Installation
1. pytorch 1.1.0
2. opencv-python
3. tensorboardX 1.7
4. yaml(pip3 install PyYAML --user)

## Dataset
- Save your annotation in the form of VOC format (in storage folder)
- Add your shop dataset in above folder or create new folder for your dataset

## Usage
1. To training. Please run the relative file in folder with model configure file, like:
    - Single training:
    `python3 train.py --cfg=./experiments/cfgs/rfb_lite_mobilenetv2_train_voc.yml`
    - Overall training:
    `python3 overall_training.py`
2. To demo with image. You just run like that:
    `python3 demo.py --p=./your_path.jpg`


## Performance and Model
- Update later

## Visualization
- Update later

## Future Work
- Implement this repository with tensorflow and try to improvement.
- Excute some other models like rentina net,..
- Addition to RepBox loss for Repulsion loss.
- test the multi-resolution traning
- add resnet, xception, ..
- figure out the bugs of visualizing on tensorboard
- Re-organize my code more professional.

## Reference
- FSSD: https://arxiv.org/pdf/1712.00960.pdf
- Repulsion loss: https://arxiv.org/pdf/1711.07752.pdf
- SSD: https://towardsdatascience.com/review-ssd-single-shot-detector-object-detection-851a94607d11
- https://medium.com/@amadeusw6/variations-of-ssd-feature-fusion-ssd-fssd-and-rainbow-ssd-rssd-beeb993f2778
- https://github.com/bailvwangzi/repulsion_loss_ssd

## authors:
1. tuanhleo11@gmail.com