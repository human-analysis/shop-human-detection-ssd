__author__ = "tuanhleo101"

import argparse
import os
import sys
from glob import glob

import cv2

from libs.inference import Detector
from libs.utils.config_parse import cfg_from_file


def parse_args():
    parser = argparse.ArgumentParser(description="Single Shot Detection")
    parser.add_argument('--cfg', dest='config_file', help='optional config file',
                        default=None, type=str)
    parser.add_argument('--path', dest='path', help='path of image that you get input of list person',
                        default=None, type=str)
    if len(sys.argv) <= 2:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args


def predict(img_path, viz_arch=False, threshold=0.6):
    detector = Detector(viz_arch)
    return detector.predict(img_path, threshold)


COLORS = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
FONT = cv2.FONT_HERSHEY_SIMPLEX


def main(viz_arch=False, threshold=0.081):
    # args = parse_args()
    # if args.config_file is not None:
    #     cfg_from_file(args.config_file)
    cfg_from_file('./cfgs/fssd_darknet19_voc.yml')
    detector = Detector(viz_arch=True)
    images_list = glob(os.path.join('./sample/images', '*.jpg'))
    total_time = 0
    for path in images_list:
        image = cv2.imread(path)
        # 4. detect
        _labels, _scores, _coords, times = detector.predict(image, threshold, check_time=True)
        #
        print(times)
        # for labels, scores, coords in zip(_labels, _scores, _coords):
        #     cv2.rectangle(image, (int(coords[0]), int(coords[1])), (int(coords[2]), int(coords[3])), COLORS[labels % 3],
        #                   2)
        #     cv2.putText(image, '{label}: {score:.3f}'.format(label='person', score=scores),
        #                 (int(coords[0]), int(coords[1])), FONT, 0.5, COLORS[labels % 3], 2)
        #
        # # 7. visualize result
        # # if args.display is True:
        # # cv2.imshow('result', image)
        # # k = cv2.waitKey(0)
        # # if k == ord('s'):
        # #     saving_path = './sample/results/' + path.split('/')[-1][:-4] + '_result' + '.jpg'
        # #     cv2.imwrite(saving_path, image)
        # # elif k == ord('n'):
        # #     continue
        # # elif k == ord('q'):
        # #     break


if __name__ == "__main__":
    # args = parse_args()
    # print(predict(args.img_path))
    main()
