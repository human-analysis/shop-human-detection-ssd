
torch-jit-export_predictp
0
base.0.conv.0.weight156 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
156
base.0.conv.1.weight
base.0.conv.1.bias
base.0.conv.1.running_mean
base.0.conv.1.running_var157 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
157158 "	LeakyRelu*
alpha���=C
158159 "MaxPool*
strides00*
pads0 0 0 0 *
kernels00r
159
base.2.conv.0.weight160 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
160
base.2.conv.1.weight
base.2.conv.1.bias
base.2.conv.1.running_mean
base.2.conv.1.running_var161 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
161162 "	LeakyRelu*
alpha���=C
162163 "MaxPool*
strides00*
pads0 0 0 0 *
kernels00r
163
base.4.conv.0.weight164 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
164
base.4.conv.1.weight
base.4.conv.1.bias
base.4.conv.1.running_mean
base.4.conv.1.running_var165 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
165166 "	LeakyRelu*
alpha���=r
166
base.5.conv.0.weight167 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00�
167
base.5.conv.1.weight
base.5.conv.1.bias
base.5.conv.1.running_mean
base.5.conv.1.running_var168 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
168169 "	LeakyRelu*
alpha���=r
169
base.5.conv.3.weight170 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
170
base.5.conv.4.weight
base.5.conv.4.bias
base.5.conv.4.running_mean
base.5.conv.4.running_var171 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
171172 "	LeakyRelu*
alpha���=C
172173 "MaxPool*
strides00*
pads0 0 0 0 *
kernels00r
173
base.7.conv.0.weight174 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
174
base.7.conv.1.weight
base.7.conv.1.bias
base.7.conv.1.running_mean
base.7.conv.1.running_var175 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
175176 "	LeakyRelu*
alpha���=r
176
base.8.conv.0.weight177 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00�
177
base.8.conv.1.weight
base.8.conv.1.bias
base.8.conv.1.running_mean
base.8.conv.1.running_var178 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
178179 "	LeakyRelu*
alpha���=r
179
base.8.conv.3.weight180 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
180
base.8.conv.4.weight
base.8.conv.4.bias
base.8.conv.4.running_mean
base.8.conv.4.running_var181 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
181182 "	LeakyRelu*
alpha���=C
182183 "MaxPool*
strides00*
pads0 0 0 0 *
kernels00s
183
base.10.conv.0.weight184 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
184
base.10.conv.1.weight
base.10.conv.1.bias
base.10.conv.1.running_mean
base.10.conv.1.running_var185 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
185186 "	LeakyRelu*
alpha���=s
186
base.11.conv.0.weight187 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00�
187
base.11.conv.1.weight
base.11.conv.1.bias
base.11.conv.1.running_mean
base.11.conv.1.running_var188 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
188189 "	LeakyRelu*
alpha���=s
189
base.11.conv.3.weight190 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
190
base.11.conv.4.weight
base.11.conv.4.bias
base.11.conv.4.running_mean
base.11.conv.4.running_var191 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
191192 "	LeakyRelu*
alpha���=s
192
base.12.conv.0.weight193 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00�
193
base.12.conv.1.weight
base.12.conv.1.bias
base.12.conv.1.running_mean
base.12.conv.1.running_var194 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
194195 "	LeakyRelu*
alpha���=s
195
base.12.conv.3.weight196 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
196
base.12.conv.4.weight
base.12.conv.4.bias
base.12.conv.4.running_mean
base.12.conv.4.running_var197 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
197198 "	LeakyRelu*
alpha���=C
198199 "MaxPool*
strides00*
pads0 0 0 0 *
kernels00s
199
base.14.conv.0.weight200 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
200
base.14.conv.1.weight
base.14.conv.1.bias
base.14.conv.1.running_mean
base.14.conv.1.running_var201 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
201202 "	LeakyRelu*
alpha���=s
202
base.15.conv.0.weight203 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00�
203
base.15.conv.1.weight
base.15.conv.1.bias
base.15.conv.1.running_mean
base.15.conv.1.running_var204 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
204205 "	LeakyRelu*
alpha���=s
205
base.15.conv.3.weight206 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
206
base.15.conv.4.weight
base.15.conv.4.bias
base.15.conv.4.running_mean
base.15.conv.4.running_var207 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
207208 "	LeakyRelu*
alpha���=s
208
base.16.conv.0.weight209 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00�
209
base.16.conv.1.weight
base.16.conv.1.bias
base.16.conv.1.running_mean
base.16.conv.1.running_var210 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
210211 "	LeakyRelu*
alpha���=s
211
base.16.conv.3.weight212 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00�
212
base.16.conv.4.weight
base.16.conv.4.bias
base.16.conv.4.running_mean
base.16.conv.4.running_var213 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test%
213214 "	LeakyRelu*
alpha���=�
182
transforms.0.conv.weight
transforms.0.conv.bias215 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00
215216 "Relu:
217OC2_DUMMY_0 "Slice*

starts0*
ends0���������(
216
OC2_DUMMY_0218 "ResizeNearest�
198
transforms.1.conv.weight
transforms.1.conv.bias219 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00
219220 "Relu:
221OC2_DUMMY_1 "Slice*

starts0*
ends0���������(
220
OC2_DUMMY_1222 "ResizeNearest�
214
transforms.2.conv.weight
transforms.2.conv.bias223 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00
223224 "Relu:
225OC2_DUMMY_2 "Slice*

starts0*
ends0���������(
224
OC2_DUMMY_2226 "ResizeNearest5
218
222
226227OC2_DUMMY_3 "Concat*
axis�
227
norm.weight
	norm.bias
norm.running_mean
norm.running_var228 "	SpatialBN*
momentumfff?*
epsilon��'7*
is_test�
228
pyramids.0.conv.weight
pyramids.0.conv.bias229 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00
229230 "Relu�
230
pyramids.1.conv.weight
pyramids.1.conv.bias231 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00
231232 "Relu�
232
pyramids.2.conv.weight
pyramids.2.conv.bias233 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00
233234 "Relu�
234
pyramids.3.conv.weight
pyramids.3.conv.bias235 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00
235236 "Relu�
236
pyramids.4.conv.weight
pyramids.4.conv.bias237 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00
237238 "Relu�
238
pyramids.5.conv.weight
pyramids.5.conv.bias239 "Conv*
strides00*
pads0 0 0 0 *
kernels00*	
group*
	dilations00
239240 "Reluv
230
loc.0.weight

loc.0.bias241 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
241242 "	Transpose*
axes0 000x
230
conf.0.weight
conf.0.bias243 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
243244 "	Transpose*
axes0 000v
232
loc.1.weight

loc.1.bias245 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
245246 "	Transpose*
axes0 000x
232
conf.1.weight
conf.1.bias247 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
247248 "	Transpose*
axes0 000v
234
loc.2.weight

loc.2.bias249 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
249250 "	Transpose*
axes0 000x
234
conf.2.weight
conf.2.bias251 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
251252 "	Transpose*
axes0 000v
236
loc.3.weight

loc.3.bias253 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
253254 "	Transpose*
axes0 000x
236
conf.3.weight
conf.3.bias255 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
255256 "	Transpose*
axes0 000v
238
loc.4.weight

loc.4.bias257 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
257258 "	Transpose*
axes0 000x
238
conf.4.weight
conf.4.bias259 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
259260 "	Transpose*
axes0 000v
240
loc.5.weight

loc.5.bias261 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
261262 "	Transpose*
axes0 000x
240
conf.5.weight
conf.5.bias263 "Conv*
strides00*
pads0000*
kernels00*	
group*
	dilations00'
263264 "	Transpose*
axes0 000
242266 "Shape
266
265267 "Gather"
267269 "
ExpandDims*
dims0 0
269
270271OC2_DUMMY_4 "Concat*
axis '
242
271272OC2_DUMMY_5 "Reshape
246274 "Shape
274
273275 "Gather"
275277 "
ExpandDims*
dims0 0
277
278279OC2_DUMMY_6 "Concat*
axis '
246
279280OC2_DUMMY_7 "Reshape
250282 "Shape
282
281283 "Gather"
283285 "
ExpandDims*
dims0 0
285
286287OC2_DUMMY_8 "Concat*
axis '
250
287288OC2_DUMMY_9 "Reshape
254290 "Shape
290
289291 "Gather"
291293 "
ExpandDims*
dims0 1
293
294295OC2_DUMMY_10 "Concat*
axis (
254
295296OC2_DUMMY_11 "Reshape
258298 "Shape
298
297299 "Gather"
299301 "
ExpandDims*
dims0 1
301
302303OC2_DUMMY_12 "Concat*
axis (
258
303304OC2_DUMMY_13 "Reshape
262306 "Shape
306
305307 "Gather"
307309 "
ExpandDims*
dims0 1
309
310311OC2_DUMMY_14 "Concat*
axis (
262
311312OC2_DUMMY_15 "ReshapeE
272
280
288
296
304
312313OC2_DUMMY_16 "Concat*
axis
244315 "Shape
315
314316 "Gather"
316318 "
ExpandDims*
dims0 1
318
319320OC2_DUMMY_17 "Concat*
axis (
244
320321OC2_DUMMY_18 "Reshape
248323 "Shape
323
322324 "Gather"
324326 "
ExpandDims*
dims0 1
326
327328OC2_DUMMY_19 "Concat*
axis (
248
328329OC2_DUMMY_20 "Reshape
252331 "Shape
331
330332 "Gather"
332334 "
ExpandDims*
dims0 1
334
335336OC2_DUMMY_21 "Concat*
axis (
252
336337OC2_DUMMY_22 "Reshape
256339 "Shape
339
338340 "Gather"
340342 "
ExpandDims*
dims0 1
342
343344OC2_DUMMY_23 "Concat*
axis (
256
344345OC2_DUMMY_24 "Reshape
260347 "Shape
347
346348 "Gather"
348350 "
ExpandDims*
dims0 1
350
351352OC2_DUMMY_25 "Concat*
axis (
260
352353OC2_DUMMY_26 "Reshape
264355 "Shape
355
354356 "Gather"
356358 "
ExpandDims*
dims0 1
358
359360OC2_DUMMY_27 "Concat*
axis (
264
360361OC2_DUMMY_28 "ReshapeE
321
329
337
345
353
361362OC2_DUMMY_29 "Concat*
axis
313364 "Shape
364
363365 "Gather"
365368 "
ExpandDims*
dims0 6
368
369
370371OC2_DUMMY_30 "Concat*
axis (
313
371372OC2_DUMMY_31 "Reshape(
362
373374OC2_DUMMY_32 "Reshape
374375 "Softmax*
axis*  :0:base.0.conv.0.weight:base.0.conv.1.weight:base.0.conv.1.bias:base.0.conv.1.running_mean:base.0.conv.1.running_var:base.2.conv.0.weight:base.2.conv.1.weight:base.2.conv.1.bias:base.2.conv.1.running_mean:base.2.conv.1.running_var:base.4.conv.0.weight:base.4.conv.1.weight:base.4.conv.1.bias:base.4.conv.1.running_mean:base.4.conv.1.running_var:base.5.conv.0.weight:base.5.conv.1.weight:base.5.conv.1.bias:base.5.conv.1.running_mean:base.5.conv.1.running_var:base.5.conv.3.weight:base.5.conv.4.weight:base.5.conv.4.bias:base.5.conv.4.running_mean:base.5.conv.4.running_var:base.7.conv.0.weight:base.7.conv.1.weight:base.7.conv.1.bias:base.7.conv.1.running_mean:base.7.conv.1.running_var:base.8.conv.0.weight:base.8.conv.1.weight:base.8.conv.1.bias:base.8.conv.1.running_mean:base.8.conv.1.running_var:base.8.conv.3.weight:base.8.conv.4.weight:base.8.conv.4.bias:base.8.conv.4.running_mean:base.8.conv.4.running_var:base.10.conv.0.weight:base.10.conv.1.weight:base.10.conv.1.bias:base.10.conv.1.running_mean:base.10.conv.1.running_var:base.11.conv.0.weight:base.11.conv.1.weight:base.11.conv.1.bias:base.11.conv.1.running_mean:base.11.conv.1.running_var:base.11.conv.3.weight:base.11.conv.4.weight:base.11.conv.4.bias:base.11.conv.4.running_mean:base.11.conv.4.running_var:base.12.conv.0.weight:base.12.conv.1.weight:base.12.conv.1.bias:base.12.conv.1.running_mean:base.12.conv.1.running_var:base.12.conv.3.weight:base.12.conv.4.weight:base.12.conv.4.bias:base.12.conv.4.running_mean:base.12.conv.4.running_var:base.14.conv.0.weight:base.14.conv.1.weight:base.14.conv.1.bias:base.14.conv.1.running_mean:base.14.conv.1.running_var:base.15.conv.0.weight:base.15.conv.1.weight:base.15.conv.1.bias:base.15.conv.1.running_mean:base.15.conv.1.running_var:base.15.conv.3.weight:base.15.conv.4.weight:base.15.conv.4.bias:base.15.conv.4.running_mean:base.15.conv.4.running_var:base.16.conv.0.weight:base.16.conv.1.weight:base.16.conv.1.bias:base.16.conv.1.running_mean:base.16.conv.1.running_var:base.16.conv.3.weight:base.16.conv.4.weight:base.16.conv.4.bias:base.16.conv.4.running_mean:base.16.conv.4.running_var:transforms.0.conv.weight:transforms.0.conv.bias:transforms.1.conv.weight:transforms.1.conv.bias:transforms.2.conv.weight:transforms.2.conv.bias:pyramids.0.conv.weight:pyramids.0.conv.bias:pyramids.1.conv.weight:pyramids.1.conv.bias:pyramids.2.conv.weight:pyramids.2.conv.bias:pyramids.3.conv.weight:pyramids.3.conv.bias:pyramids.4.conv.weight:pyramids.4.conv.bias:pyramids.5.conv.weight:pyramids.5.conv.bias:norm.weight:	norm.bias:norm.running_mean:norm.running_var:loc.0.weight:
loc.0.bias:loc.1.weight:
loc.1.bias:loc.2.weight:
loc.2.bias:loc.3.weight:
loc.3.bias:loc.4.weight:
loc.4.bias:loc.5.weight:
loc.5.bias:conf.0.weight:conf.0.bias:conf.1.weight:conf.1.bias:conf.2.weight:conf.2.bias:conf.3.weight:conf.3.bias:conf.4.weight:conf.4.bias:conf.5.weight:conf.5.bias:359:351:369:335:327:319:310:305:265:363:294:370:289:270:346:354:343:281:278:373:338:297:221:273:330:217:286:225:302:314:322B372B375