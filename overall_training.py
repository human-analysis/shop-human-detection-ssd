__author__ = "tuanhleo101"

import os
from glob import glob

import cv2

from libs.inference import Detector
from libs.ssds_train import train_model
from libs.utils.config_parse import cfg_from_file
from libs.utils.dataset import preparing


def performance_overall(net_analysis):
    darknet53, darknet19, vgg16, mobilenetv1, mobilenetv2 = net_analysis
    print("\n                         PERFORMANCE\n")
    print('-------------:---------------:---------------:---------------.')
    print('             |     YOLOV2    |     YOLOV3    |      FSSD     |')
    print('-------------:---------------:---------------:---------------:')
    print('  Darknet53  |    ', darknet53[0][0], '    |    ', darknet53[1][0], '    |    ', darknet53[2][0], '    |')
    print('-------------:---------------:---------------:---------------:')
    print('  Darknet19  |    ', darknet19[0][0], '    |    ', darknet19[1][0], '    |    ', darknet19[2][0], '    |')
    print('-------------:---------------:---------------:---------------:')
    print('    VGG16    |    ', vgg16[0][0], '    |    ', vgg16[1][0], '    |    ', vgg16[2][0], '    |')
    print('-------------:---------------:---------------:---------------:')
    print(' Mobilenetv1 |    ', mobilenetv1[0][0], '    |    ', mobilenetv1[1][0], '    |    ', mobilenetv1[2][0],
          '    |')
    print('-------------:---------------:---------------:---------------:')
    print(' Mobilenetv2 |    ', mobilenetv2[0][0], '    |    ', mobilenetv2[1][0], '    |    ', mobilenetv2[2][0],
          '    |')
    print('-------------:---------------:---------------:---------------:\n')

    print("                                             INFER TIME\n")
    print('-------------:---------------------------:-----------------------------:------------------------------.')
    print('             |           YOLOV2          |            YOLOV3           |             FSSD             |')
    print('-------------:---------------------------:-----------------------------:------------------------------:')
    print('  Darknet53  |  ', darknet53[0][1][0], '  ||  ', darknet53[0][1][1], '   |   ', darknet53[1][1][0], '  ||  ',
          darknet53[1][1][1], '    |    ', darknet53[2][1][0], '  ||  ', darknet53[2][1][1], '    |')
    print('-------------:---------------------------:-----------------------------:------------------------------:')
    print('  Darknet19  |  ', darknet19[0][1][0], '  ||  ', darknet19[0][1][1], '   |   ', darknet19[1][1][0], '  ||  ',
          darknet19[1][1][1], '    |    ', darknet19[2][1][0], '  ||  ', darknet19[2][1][1], '    |')
    print('-------------:---------------------------:-----------------------------:------------------------------:')
    print('    VGG16    |  ', vgg16[0][1][0], '  ||  ', vgg16[0][1][1], '   |   ', vgg16[1][1][0], '  ||  ',
          vgg16[1][1][1], '    |    ', vgg16[2][1][0], '  ||  ', vgg16[2][1][1], '    |')
    print('-------------:---------------------------:-----------------------------:------------------------------:')
    print(' Mobilenetv1 |  ', mobilenetv1[0][1][0], '  ||  ', mobilenetv1[0][1][1], '   |   ', mobilenetv1[1][1][0],
          '  ||  ', mobilenetv1[1][1][1], '    |    ', mobilenetv1[2][1][0], '  ||  ', mobilenetv1[2][1][1], '    |')
    print('-------------:---------------------------:-----------------------------:------------------------------:')
    print(' Mobilenetv2 |  ', mobilenetv2[0][1][0], '  ||  ', mobilenetv2[0][1][1], '   |   ', mobilenetv2[1][1][0],
          '  ||  ', mobilenetv2[1][1][1], '    |    ', mobilenetv2[2][1][0], '  ||  ', mobilenetv2[2][1][1], '    |')
    print('-------------:---------------------------:-----------------------------:------------------------------:\n')


def perfomance_analysis():
    config_mapping = {
        'mobilenetv1_fssd': 'cfgs/fssd_lite_mobilenetv1_train_voc.yml',
        'mobilenetv2_fssd': 'cfgs/fssd_lite_mobilenetv2_train_voc.yml',
        'darknet53_fssd': 'cfgs/fssd_darknet53_voc.yml',
        'darknet19_fssd': 'cfgs/fssd_darknet19_voc.yml',
        'mobilenetv1_yolov2': 'cfgs/yolo_v2_mobilenetv1_voc.yml',
        'mobilenetv1_yolov3': 'cfgs/yolo_v3_mobilenetv1_voc.yml',
        'mobilenetv2_yolov2': 'cfgs/yolo_v2_mobilenetv2_voc.yml',
        'mobilenetv2_yolov3': 'cfgs/yolo_v3_mobilenetv2_voc.yml',
        'darknet53_yolov3': 'cfgs/yolo_v3_darknet53_voc.yml',
        'darknet19_yolov2': 'cfgs/yolo_v2_darknet19_voc.yml',
        'vgg16_fssd': 'cfgs/fssd_vgg16_train_voc.yml',
    }
    backbones = ['vgg16', 'mobilenetv1', 'mobilenetv2', 'darknet19', 'darknet53', ]
    detectors = ['fssd', 'yolov3', 'yolov2']
    image_samples = glob(os.path.join(os.path.abspath('.'), 'sample', '*.jpg'))
    net_analysis = []

    for backbone in backbones:
        temp = [0.0, [0.0, 0.0]]
        for detector in detectors:
            if backbone + "_" + detector in config_mapping.keys():
                print('|====================================================|')
                print('| Backbone: ', backbone, ' detector: ', detector, '|\n')
                cfg_from_file(config_mapping[backbone + "_" + detector])
                train_model(temp[0])
                inferencer = Detector()
                if len(image_samples) != 0:
                    temp[1][1] = inference(inferencer, image_samples)

                print("| Finishing ", backbone, '_', detector, ' training.|')
                print('|====================================================|')
                net_analysis.append(temp)
                del inferencer

    print('The result display')
    performance_overall(net_analysis)


def inference(detector, image_paths):
    total_time_average = 0
    for path in image_paths:
        img = cv2.imread(path)
        labels, scores, coords, (total_time, preprocess_time, net_forward_time, detect_time, output_time) = \
            detector.predict(img, check_time=True)
        total_time_average += total_time

    total_time_average = total_time_average / len(image_paths)
    return total_time_average


if __name__ == '__main__':
    # darknet53 = [[12345, [123456, 123456]], [12345, [123456, 123456]], [12345, [123456, 123456]]]
    # darknet19 = [[12345, [123456, 123456]], [12345, [123456, 123456]], [12345, [123456, 123456]]]
    # vgg16 = [[12345, [123456, 123456]], [12345, [123456, 123456]], [12345, [123456, 123456]]]
    # mobilenetv1 = [[12345, [123456, 123456]], [12345, [123456, 123456]], [12345, [123456, 123456]]]
    # mobilenetv2 = [[12345, [123456, 123456]], [12345, [123456, 123456]], [12345, [123456, 123456]]]
    # # print(darknet53)
    # performance_overall([darknet53, darknet19, vgg16, mobilenetv1, mobilenetv2])
    preparing()
    perfomance_analysis()
