import time
from glob import glob

import cv2
import numpy as np
from skimage.feature import hog

COLORS = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
FONT = cv2.FONT_HERSHEY_SIMPLEX

human = glob('storage/shop/person/pos_person/*')
nothuman = glob('storage/shop/person/neg_person/*.jpg')
p_human = glob('storage/shop/processed_person/pos_person/*')
p_nothuman = glob('storage/shop/processed_person/neg_person/*.jpg')
processed_folder = 'storage/shop/processed_person/'
processed_human = processed_folder + 'pos_person/'
processed_nothuman = processed_folder + 'neg_person/'
test = glob('sample/test_image/*.jpg')


# Define a function to return HOG features and visualization
def get_hog_features(img, orient, pix_per_cell, cell_per_block,
                     Visualize=False, feature_vec=True):
    # if visualize is true than it will return features with hog image, else it will return features only
    if Visualize == True:
        features, hog_image = hog(img, orientations=orient,
                                  pixels_per_cell=(pix_per_cell, pix_per_cell),
                                  cells_per_block=(cell_per_block, cell_per_block),
                                  transform_sqrt=True,
                                  visualize=Visualize, feature_vector=feature_vec)
        return features, hog_image
    else:
        features = hog(img, orientations=orient,
                       pixels_per_cell=(pix_per_cell, pix_per_cell),
                       cells_per_block=(cell_per_block, cell_per_block),
                       transform_sqrt=True,
                       visualize=Visualize, feature_vector=feature_vec)
        # print("hog:", features.shape)
        # print(img.shape)
        return features


# Resizing image to 32 by 32 size and taking it as feature
def get_spatial_features(img, size=(32, 32)):
    return cv2.resize(img, size).ravel()


# returning histograms as feature in all channels
def get_hist_features(img, nbins=32, bins_range=(0, 256)):
    channel1_hist = np.histogram(img[:, :, 0], bins=nbins, range=bins_range)
    channel2_hist = np.histogram(img[:, :, 1], bins=nbins, range=bins_range)
    channel3_hist = np.histogram(img[:, :, 2], bins=nbins, range=bins_range)

    return np.concatenate((channel1_hist[0], channel2_hist[0], channel3_hist[0]))


def extract_features(imgs, color_space='RGB', spatial_size=(32, 32),
                     hist_bins=32, orient=9,
                     pix_per_cell=8, cell_per_block=2, hog_channel=0,
                     spatial_feat=True, hist_feat=True, hog_feat=True):
    # Create a list to append feature vectors to
    features = []
    l = 0
    # Iterate through the list of images
    for file in imgs:
        file_features = []

        # Read in each one by one
        image = cv2.imread(file)
        # Sift low level features
        #         gray= cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        #         sift = cv2.xfeatures2d.SIFT_create()
        #         kp ,descp= sift.detectAndCompute(image, None)
        #         descp=descp.reshape(-1)

        #         print(descp.shape)
        #         file_features.append(descp)
        # apply color conversion if other than 'RGB'
        feature_image = np.copy(image)

        if spatial_feat == True:
            spatial_features = get_spatial_features(feature_image, size=spatial_size)
            # print(spatial_features.shape)
            # print(len(spatial_features))
            file_features.append(spatial_features)

        if hist_feat == True:
            hist_features = get_hist_features(feature_image, nbins=hist_bins)
            # print("hist")
            # print(len(hist_features))
            file_features.append(hist_features)

        if hog_feat == True:
            if hog_channel == 'ALL':  # all implies for all channels
                hog_features = []
                for channel in range(feature_image.shape[2]):
                    hog_features.append(get_hog_features(feature_image[:, :, channel],
                                                         orient, pix_per_cell, cell_per_block,
                                                         Visualize=False, feature_vec=True))
                #                     print("HOG:", len(hog_features[0]))
                hog_features = np.ravel(hog_features)
            else:
                hog_features = get_hog_features(feature_image[:, :, hog_channel], orient,
                                                pix_per_cell, cell_per_block, Visualize=False, feature_vec=True)

            #            print(len(hog_features))
            file_features.append(hog_features)
            # print("HOG:", len(hog_features))

        features.append(np.concatenate(file_features))
    # Return list of feature vectors
    print("Feature Vector Length", len(features[0]))

    return features


if __name__ == "__main__":
    ### Defining parameters
    color_space = 'RGB'
    orient = 9  # HOG orientations
    pix_per_cell = 8  # HOG pixels per cell
    cell_per_block = 2  # HOG cells per block
    hog_channel = 'ALL'  # We are taking hog features in all colors
    spatial_size = (16, 16)  # Spatial binning dimensions
    hist_bins = 16  # Number of histogram bins
    spatial_feat = False  # Taking spatial features
    hist_feat = True  # Taking histogram features
    hog_feat = True  # Taking hog features
    y_start_stop = [400, None]

    human_features = extract_features(p_human, color_space=color_space,
                                      spatial_size=spatial_size, hist_bins=hist_bins,
                                      orient=orient, pix_per_cell=pix_per_cell,
                                      cell_per_block=cell_per_block,
                                      hog_channel=hog_channel, spatial_feat=spatial_feat,
                                      hist_feat=hist_feat, hog_feat=hog_feat)
    nothuman_features = extract_features(p_nothuman, color_space=color_space,
                                         spatial_size=spatial_size, hist_bins=hist_bins,
                                         orient=orient, pix_per_cell=pix_per_cell,
                                         cell_per_block=cell_per_block,
                                         hog_channel=hog_channel, spatial_feat=spatial_feat,
                                         hist_feat=hist_feat, hog_feat=hog_feat)

    # scaled_X = scaler.transform(X)
    #
    subimg = np.random.rand(64, 64, 3)
    hog1 = get_hog_features(subimg[:, :, 0], orient, pix_per_cell, cell_per_block, feature_vec=True).ravel()
    hog2 = get_hog_features(subimg[:, :, 1], orient, pix_per_cell, cell_per_block, feature_vec=True).ravel()
    hog3 = get_hog_features(subimg[:, :, 2], orient, pix_per_cell, cell_per_block, feature_vec=True).ravel()
    hog_features = np.hstack((hog1, hog2, hog3))
    hist_features = get_hist_features(subimg, nbins=hist_bins)
    test_features = scaler.transform(np.hstack((hog_features, hist_features)).reshape(1, -1))

    start = time.time()
    human_prob = svc.predict_proba(test_features)
    print(time.time() - start)
