import os
from glob import glob
from xml.etree import ElementTree as ET

import cv2
import numpy as np
from tqdm import tqdm

from libs.deep_sort.tools.generate_detections import create_box_encoder


def parse_rec(filename, base):
    tree = ET.parse(filename)
    path = filename.split('/')[-1][:-4] + '.jpg'
    image_path = os.path.join(base, path)
    frame = cv2.imread(image_path)
    boxes = []
    labels = []
    for obj in tree.findall('object'):
        bbox = obj.find('bndbox')
        ix = int(bbox.find('xmin').text)
        iy = int(bbox.find('ymin').text)
        ex = int(bbox.find('xmax').text)
        ey = int(bbox.find('ymax').text)
        boxes.append([ix, iy, ex - ix, ey - iy])
        img = frame[iy:ey, ix:ex, :]
        cv2.imshow('img', img)
        k = cv2.waitKey(0)

        if k == ord('y'):
            label = 1
            labels.append(label)
        elif k == ord('q'):
            label = 0
            labels.append(label)
            continue

    return frame, boxes, labels


def main(root, data_dir):
    xml_files = glob(os.path.join(root, data_dir, '*.xml'))
    datas = []
    if os.path.isfile(os.path.join(root, 'data.npy')):
        datas = np.load(os.path.join(root, 'data.npy'))
    else:
        for filename in tqdm(xml_files):
            img, boxes, labels_ = parse_rec(filename, os.path.join(root, data_dir))
            features = encoder(img, boxes)
            for i in range(features.shape[0]):
                datas.append([features[i], labels_[i]])

        np.save(os.path.join(root, 'data.npy'), np.array(datas))

    return datas


if __name__ == "__main__":
    model_filename = "storage/tracking_model/mars-small128.pb"
    encoder = create_box_encoder(model_filename, batch_size=4096)
    datas = main(os.path.abspath('.') + '/storage/shop', 'data')
    print(datas.shape)
