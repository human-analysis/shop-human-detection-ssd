#! /usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

import datetime
import errno
import os
import warnings
from timeit import time

import cv2
import numpy as np
from PIL import ImageTk, Image
from libs.detector.detector import Detector
from tracker.tracker import Tracker_temp

warnings.filterwarnings('ignore')
IS_DETECTION_DISPLAY = False
IS_TRACKING_DISPLAY = True
ix, iy, ex, ey = -1, -1, -1, -1


def draw_rec(event, x, y, flags, param):
    global ix, iy, ex, ey, drawing, mode
    if event == cv2.EVENT_LBUTTONDOWN:
        ix, iy = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        ex, ey = x, y
        cv2.rectangle(param, (ix, iy), (x, y), (0, 255, 0), 3)


track_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0),
                (0, 255, 255), (255, 0, 255), (255, 127, 255),
                (127, 0, 255), (127, 0, 127)]


def get_crop_size(cap, down_sample_ratio):
    while cap.isOpened():
        ret, frame = cap.read()
        (h, w) = frame.shape[:2]
        frame = cv2.resize(frame, (int(w * down_sample_ratio), int(h * down_sample_ratio)))
        cv2.namedWindow('draw_rectangle')
        cv2.setMouseCallback('draw_rectangle', draw_rec, frame)
        print('Choose your area of interect!')
        while 1:
            cv2.imshow('draw_rectangle', frame)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('a'):
                cv2.destroyAllWindows()
                break
        break
    # print(ix, iy, ex, ey)


def testIntersectionIn(x, y):
    global ix, iy, ex, ey
    # a = iy - iy
    # b = iy
    res = y - iy
    if res > 1.0:
        return True
    return False


class tracking_by_detection(object):
    def __init__(self, detector_name, tracker_name, config_path='./config.cfg'):
        self.det = Detector(detector_name, config_path)
        self.tra = Tracker_temp(tracker_name, config_path)
        # self.region = region

    def open_with_mkdir(self, path):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as e:
            if e.errno == errno.EEXIST and os.path.isdir(os.path.dirname(path)):
                pass
            else:
                raise

        return open(path, 'w')

    def new_tk_image(self, frame, size):
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        image = Image.fromarray(image).resize(size)
        imageTk = ImageTk.PhotoImage(image=image)
        return imageTk

    def tracking_by_detection(self, video_stream, output_file, show_image=True, detect_freq=2, down_sample_ratio=1.0,
                              is_probability_driven_detect=True, print_fps=False, trace=False,
                              ratio_filter=1.73, confidence_increation=0.0, location_variation=0.0,
                              video_frame=None, size_display=None):
        video_capture = cv2.VideoCapture(video_stream)
        get_crop_size(video_capture, down_sample_ratio)

        fps = 0.0
        step_counter = 0
        counter = 0
        first_time_flag = True
        start_time = time.time()
        total_time = time.time()
        result_list = []
        frame_index = 1
        textIn = 0
        textOut = 0
        while True:
            ret, frame = video_capture.read()
            if ret != True:
                break
            (h, w) = frame.shape[:2]
            frame_resided = cv2.resize(frame, (int(w * down_sample_ratio), int(h * down_sample_ratio)))
            if (step_counter % detect_freq == 0) or counter == 0 or (
                    is_probability_driven_detect == True and self.tra.is_detection_needed() == True):
                results = self.det.detect_image_frame(frame_resided, to_xywh=True, ratio_filter=1.73)
                boxes = np.array([result[1:5] for result in results])
                for box in boxes:
                    box[0] = max(0, box[0] - location_variation)
                if ratio_filter != 1.0:
                    for box in boxes:
                        box[3] = min(box[3], box[2] * ratio_filter)
                boxes = np.array([box for box in boxes if box[2] < w / 3])
                scores = np.array([min(1.0, result[5] + confidence_increation) for result in results])
                self.tra.set_detecion_needed(False)

            tracker, detections = self.tra.start_tracking(frame_resided, boxes, scores)
            # Call the tracker
            if (IS_TRACKING_DISPLAY is True):
                for track in tracker.tracks:
                    if track.is_confirmed() and track.time_since_update > 1:
                        continue
                    bbox = track.to_tlbr()
                    cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                  (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), (255, 255, 255),
                                  2)
                    cv2.putText(frame, str(track.track_id),
                                (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)), 0, 5e-3 * 200,
                                (0, 255, 0), 2)
                    # bbox = track.to_tlwh()
                    centroid = (bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2
                    text = "ID {}".format(track.track_id)
                    # cv2.putText(frame, text, (int(centroid[0] - 10), int(centroid[1] - 10)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

                    # cv2.putText(frame, str(track.track_id),
                    #             (int((centroid[1]) / down_sample_ratio), int((centroid[0])/down_sample_ratio)), 0, 5e-3 * 200,
                    #             (0, 255, 0), 2)
                    # cv2.circle(frame, (int(centroid[0]), int(centroid[1])), 4, (0, 255, 0), -1)
                    # cv2.putText(frame, str(int(centroid[1])),(int(centroid[0]), int(centroid[1])),0, 5e-3 * 200, (0,255,0),2)
                    if trace and len(track.prev_box) >= 2:
                        for i in range(0, len(track.prev_box) - 1):
                            # Draw trace line
                            x1 = (track.prev_box[i][0] + track.prev_box[i][2]) / 2
                            y1 = (track.prev_box[i][1] + track.prev_box[i][3]) / 2
                            x2 = (track.prev_box[i + 1][0] + track.prev_box[i + 1][2]) / 2
                            y2 = (track.prev_box[i + 1][1] + track.prev_box[i + 1][3]) / 2
                            clr = track.track_id % 9
                            cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)),
                                     track_colors[clr], 2)

                    result_list.append(
                        [frame_index, track.track_id, bbox[0] / down_sample_ratio, bbox[1] / down_sample_ratio,
                         bbox[2] / down_sample_ratio, bbox[3] / down_sample_ratio])

            for track in tracker.tracks:
                if track.is_confirmed() and track.time_since_update > 1:
                    continue
                bbox = track.to_tlbr()
                centroid = (bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2
                text = "ID {}".format(track.track_id)
                cv2.circle(frame, (int(centroid[0]), int(centroid[1])), 4, (0, 255, 0), -1)
                # cv2.putText(frame, str(int(centroid[1])), (int(centroid[0]), int(centroid[1])), 0, 5e-3 * 200,
                #             (0, 255, 0), 2)
                if len(track.to_centroids()):
                    y = [c[1] for c in track.to_centroids()]
                    orientation = centroid[1] - np.mean(y) - 0
                    # print(orientation, centroid, ey)
                    if not track.counted:
                        if orientation > 1.0 and (centroid[1] <= ey and centroid[1] > iy):
                            textIn += 1
                            track.counted = True
                        elif orientation < 1.0 and (centroid[1] >= iy and centroid[1] < ey):
                            textOut += 1
                            track.counted = True
            #                         print(len(track.to_centroids()))
            # if len(track.to_centroids()) > 2:
            #     current_x, current_y = track.to_centroids()[-1]
            #     prev_x, prev_y = track.to_centroids()[-2]aq
            #     # print(prev_x, prev_y)
            #     #                           print(len(track.to_centroids()))
            #     cv2.line(frame, (int(prev_x), int(prev_y)),
            #              (int(current_x), int(current_y)), (0, 255, 0), 3)

            # print(textIn, textOut)
            if (IS_DETECTION_DISPLAY is True):
                for detection in detections:
                    bbox = detection.to_tlbr()
                    cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                  (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), (255, 0, 0), 2)

            counter += 1
            step_counter += 1
            if (step_counter % detect_freq == 0):
                fps = step_counter / (time.time() - start_time)
                if (print_fps is True):
                    print(fps)
                step_counter = 0
                cv2.putText(frame, 'FPS:' + str(round(fps, 1)), (0, 100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                start_time = time.time()
                if (first_time_flag is True):
                    step_counter = 0
                    counter = 0
                    total_time = time.time()
                    first_time_flag = False

            cv2.line(frame, (int(ix), int(iy)), (int(ex), int(iy)), (0, 255, 0), 2)
            cv2.putText(frame, str(int(iy)), (int(ix), int(iy)), 0, 5e-3 * 200, (0, 255, 0), 2)

            cv2.line(frame, (int(ix), int(ey)), (int(ex), int(ey)), (255, 0, 0), 2)
            cv2.putText(frame, str(int(ey)), (int(ix), int(ey)), 0, 5e-3 * 200, (255, 0, 0), 2)

            font = cv2.FONT_HERSHEY_COMPLEX_SMALL
            left = 0
            top = 0
            ret, baseline = cv2.getTextSize('Leaver: ' + str(textOut), fontFace=font, fontScale=1.3, thickness=1)
            cv2.rectangle(frame, (left, top), (left + ret[0], top + ret[1] + baseline),
                          color=(122, 255, 33), thickness=-1)
            cv2.putText(frame, 'Leaver: ' + str(textOut), (left, top + ret[1] + baseline), font, 0.7, (0, 0, 0), 1,
                        cv2.LINE_AA)

            text = 'Inner: ' + str(textIn)
            x = 30
            y = 30
            i = 1

            left = 0
            top = y
            cv2.rectangle(frame, (left, top), (left + ret[0], top + ret[1] + baseline),
                          color=(255, 255, 255), thickness=-1)
            cv2.putText(frame, text, (left, top + ret[1] + baseline), font, 0.7, (0, 0, 0), 1, cv2.LINE_AA)
            cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                        (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

            if video_frame:
                show_image = False
                imageTk = self.new_tk_image(frame, size_display)
                video_frame.imgtk = imageTk
                video_frame.configure(image=imageTk)
                video_frame.after(10)
            if (show_image == True):
                cv2.imshow('image', frame)
            frame_index += 1

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        fps = counter / (time.time() - total_time)
        print('Average FPS:', round(fps, 1))
        print('Total eplased:', round(time.time() - total_time, 2))

        try:
            f = self.open_with_mkdir(output_file)
            for result in result_list:
                print('%d,%d,%.2f,%.2f,%.2f,%.2f,1,-1,-1,-1' % (
                    int(result[0]), int(result[1]), float(result[2]), float(result[3]), float(result[4]),
                    float(result[5])),
                      file=f)
            f.close()
        except Exception as e:
            print(e)
            print('Something went wrong when writing output file!')

        video_capture.release()
        cv2.destroyAllWindows()
        try:
            del result_list
            del video_capture
            del frame
            del boxes
            del scores
            del step_counter
            del first_time_flag
            del start_time
            del total_time
            del frame_index
        except Exception as e:
            print(e)
        return video_frame


if __name__ == '__main__':
    detector_name = 'squeezenetv1_0'
    tracker_name = 'deep_sort'

    tra_by_det = tracking_by_detection(detector_name, tracker_name)

    fps, nb_frames = tra_by_det.tracking_by_detection(video_stream='./_samples/counting.mp4',
                                                      output_file='fps.txt', show_image=True, detect_freq=2,
                                                      down_sample_ratio=1.0, is_probability_driven_detect=True,
                                                      trace=True, ratio_filter=3.21)

    # print(str(sum([fps * nb_frames for fps, nb_frames in zip(fps_list, nb_frames_list)]) / sum(nb_frames_list)))
