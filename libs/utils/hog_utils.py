import cv2
import matplotlib.pyplot as plt
import numpy as np
from skimage.feature import hog


# Define a function to return HOG features and visualization
def get_hog_features(img, orient, pix_per_cell, cell_per_block,
                     Visualise=False, feature_vec=True):
    # if visualize is true than it will return features with hog image, else it will return features only
    if Visualise == True:
        features, hog_image = hog(img, orientations=orient,
                                  pixels_per_cell=(pix_per_cell, pix_per_cell),
                                  cells_per_block=(cell_per_block, cell_per_block),
                                  transform_sqrt=True,
                                  visualise=Visualise, feature_vector=feature_vec)
        return features, hog_image
    else:
        features = hog(img, orientations=orient,
                       pixels_per_cell=(pix_per_cell, pix_per_cell),
                       cells_per_block=(cell_per_block, cell_per_block),
                       transform_sqrt=True,
                       visualise=Visualise, feature_vector=feature_vec)
        # print("hog:", features.shape)
        # print(img.shape)
        return features


# Resizing image to 32 by 32 size and taking it as feature
def get_spatial_features(img, size=(32, 32)):
    return cv2.resize(img, size).ravel()


# returning histograms as feature in all channels
def get_hist_features(img, nbins=32, bins_range=(0, 256)):
    channel1_hist = np.histogram(img[:, :, 0], bins=nbins, range=bins_range)
    channel2_hist = np.histogram(img[:, :, 1], bins=nbins, range=bins_range)
    channel3_hist = np.histogram(img[:, :, 2], bins=nbins, range=bins_range)

    return np.concatenate((channel1_hist[0], channel2_hist[0], channel3_hist[0]))


def extract_features(imgs, color_space='RGB', spatial_size=(32, 32),
                     hist_bins=32, orient=9,
                     pix_per_cell=8, cell_per_block=2, hog_channel=0,
                     spatial_feat=True, hist_feat=True, hog_feat=True):
    # Create a list to append feature vectors to
    features = []
    l = 0
    # Iterate through the list of images
    for file in imgs:
        file_features = []

        # Read in each one by one
        image = cv2.imread(file)
        # Sift low level features
        #         gray= cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
        #         sift = cv2.xfeatures2d.SIFT_create()
        #         kp ,descp= sift.detectAndCompute(image, None)
        #         descp=descp.reshape(-1)

        #         print(descp.shape)
        #         file_features.append(descp)
        # apply color conversion if other than 'RGB'
        feature_image = np.copy(image)

        if spatial_feat == True:
            spatial_features = get_spatial_features(feature_image, size=spatial_size)
            # print(spatial_features.shape)
            # print(len(spatial_features))
            file_features.append(spatial_features)

        if hist_feat == True:
            hist_features = get_hist_features(feature_image, nbins=hist_bins)
            # print("hist")
            # print(len(hist_features))
            file_features.append(hist_features)

        if hog_feat == True:
            if hog_channel == 'ALL':  # all implies for all channels
                hog_features = []
                for channel in range(feature_image.shape[2]):
                    hog_features.append(get_hog_features(feature_image[:, :, channel],
                                                         orient, pix_per_cell, cell_per_block,
                                                         Visualise=False, feature_vec=True))
                #                     print("HOG:", len(hog_features[0]))
                hog_features = np.ravel(hog_features)
            else:
                hog_features = get_hog_features(feature_image[:, :, hog_channel], orient,
                                                pix_per_cell, cell_per_block, Visualise=False, feature_vec=True)

            #            print(len(hog_features))
            file_features.append(hog_features)
            # print("HOG:", len(hog_features))

        features.append(np.concatenate(file_features))
        # Return list of feature vectors
        print("Feature Vector Length", len(features[0]))

        return features

    # Define a function to draw bounding boxes


def draw_boxes(img, bboxes, color=(0, 0, 255), thick=6):
    imcopy = np.copy(img)
    # traversing through all the bounding boxes and drawing them
    for bbox in bboxes:
        cv2.rectangle(imcopy, bbox[0], bbox[1], color, thick)
    # Return the image copy with boxes drawn
    return imcopy


def display(humans, nothumans):
    fig, axs = plt.subplots(4, 8, figsize=(16, 8))
    fig.subplots_adjust(hspace=.2, wspace=.001)
    axs = axs.ravel()

    for i in np.arange(16):
        img = cv2.imread(humans[np.random.randint(0, len(humans))])
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        axs[i].axis('off')
        axs[i].set_title('Human', fontsize=10)
        axs[i].imshow(img)

    for i in np.arange(16, 32):
        img = cv2.imread(nothumans[np.random.randint(0, len(nothumans))])
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        axs[i].axis('off')
        axs[i].set_title('Not Human', fontsize=10)
        axs[i].imshow(img)
