import argparse
import os
import sys
from glob import glob
from xml.etree import ElementTree as ET

import cv2
from tqdm import tqdm


def parse_rec(filename, base, saving_dir):
    tree = ET.parse(filename)
    path = filename.split('/')[-1][:-4] + '.jpg'
    image_path = os.path.join(base, path)
    img = cv2.imread(image_path)
    cnt = 0
    for obj in tree.findall('object'):
        bbox = obj.find('bndbox')
        ix = int(bbox.find('xmin').text)
        iy = int(bbox.find('ymin').text)
        ex = int(bbox.find('xmax').text)
        ey = int(bbox.find('ymax').text)
        person_img = img[iy:ey, ix:ex, :]
        cv2.imwrite(saving_dir + '/' + filename.split('/')[-1][:-4] + '_' + str(cnt) + '.jpg', person_img)
        cnt += 1


def main(root, data_dir, saving_data):
    xml_files = glob(os.path.join(root, data_dir, '*.xml'))
    saving_data = os.path.join(root, saving_data)
    for path in tqdm(xml_files):
        parse_rec(path, os.path.join(root, data_dir), saving_data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate datasets for salesperson recognition")
    parser.add_argument("--r", default=os.path.abspath('.') + '/storage/shop')
    parser.add_argument("--d", default='data')
    parser.add_argument("--s", default='person')

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    main(args.r, args.d, args.s)
