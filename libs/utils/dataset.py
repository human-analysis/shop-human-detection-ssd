def data_preprocessing(train_file, classes_file):
    image_paths, targets = [], []
    data_lines = open(train_file, 'r').readlines()
    classes = open(classes_file, 'r').readlines()
    mapping_label = {'__BACKGROUND__': 0}
    for i, label in enumerate(classes):
        mapping_label[label] = i + 1

    for line in data_lines:
        line = line.strip('\n')
        elems = line.split(' ')
        image_paths.append(elems[0])
        temp = []
        for i in range(1, len(elems), 5):
            xmin = float(elems[i])
            ymin = float(elems[i + 1])
            xmax = float(elems[i + 2])
            ymax = float(elems[i + 3])
            label = elems[i + 4]
            temp.append([xmin, ymin, xmax, ymax, mapping_label[label.rstrip('\n')]])
        targets.append(temp)

    return image_paths, targets, mapping_label


def preparing():
    import xml.etree.ElementTree as ET
    from glob import glob
    import os
    import random
    root = os.path.abspath('.') + '/storage/shop'
    data_dir = 'data'
    xml_files = glob(os.path.join(root, data_dir, '*.xml'))
    random.shuffle(xml_files)
    n = len(xml_files)
    train_files = xml_files[:int(n * 0.9)]
    test_files = xml_files[int(n * 0.9):]

    os.remove("storage/shop/train.txt")
    os.remove("storage/shop/val.txt")

    txt_train = open(os.path.join(root, 'train.txt'), 'w')
    txt_test = open(os.path.join(root, 'val.txt'), 'w')

    def parse_rec(filename, txt_file, i, k):
        s = ''
        tree = ET.parse(filename)
        objects = []
        path = filename.split('/')[-1][:-4] + '.jpg'
        image_path = os.path.join(root, data_dir, path)
        s += image_path + ' '
        for obj in tree.findall('object'):
            obj_struct = []
            bbox = obj.find('bndbox')
            obj_struct = [int(bbox.find('xmin').text),
                          int(bbox.find('ymin').text),
                          int(bbox.find('xmax').text),
                          int(bbox.find('ymax').text)]
            s += bbox.find('xmin').text + ' ' + bbox.find('ymin').text + ' ' + bbox.find('xmax').text + ' ' + bbox.find(
                'ymax').text + ' person' + ' '
            objects.append(obj_struct)
        s = s[:-1]
        if i != k:
            txt_file.write(s + '\n')
        else:
            txt_file.write(s)
        return True

    k = len(train_files)
    for i, file in enumerate(train_files):
        parse_rec(file, txt_train, i, k - 1)

    k = len(test_files)
    for i, file in enumerate(test_files):
        parse_rec(file, txt_test, i, k - 1)
