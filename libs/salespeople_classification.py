import os
from glob import glob
from xml.etree import ElementTree as ET

import cv2

from libs.deep_sort.tools.generate_detections import create_box_encoder


def parse_rec(filename, base):
    tree = ET.parse(filename)
    path = filename.split('/')[-1][:-4] + '.jpg'
    image_path = os.path.join(base, path)
    img = cv2.imread(image_path)
    boxes = []
    for obj in tree.findall('object'):
        bbox = obj.find('bndbox')
        ix = int(bbox.find('xmin').text)
        iy = int(bbox.find('ymin').text)
        ex = int(bbox.find('xmax').text)
        ey = int(bbox.find('ymax').text)
        boxes.append([ix, iy, ex - ix, ey - iy])

    return img, boxes


def main(root, data_dir, encoder):
    xml_files = glob(os.path.join(root, data_dir, '*.xml'))
    img, boxes = parse_rec(xml_files[0], os.path.join(root, data_dir))
    features = encoder(img, boxes)
    print(features)


if __name__ == "__main__":
    model_filename = "storage/tracking_model/mars-small128.pb"
    encoder = create_box_encoder(model_filename, batch_size=4096)
    main(os.path.abspath('.') + '/storage/shop', 'shop', encoder)
