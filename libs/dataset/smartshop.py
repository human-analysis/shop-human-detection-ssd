import cv2
import numpy as np
import torch.utils.data as data

from libs.utils.dataset import data_preprocessing


class ShopDataset(data.Dataset):
    def __init__(self, train_file, mapping_file, preproc=None, target_transform=None):
        self.train_file = train_file
        self.preproc = preproc
        self.target_transforms = target_transform

        self.image_paths, self.target_lists, self.mapping_label = data_preprocessing(train_file, mapping_file)
        self.n_data = len(self.image_paths)

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, index):

        image = cv2.imread(self.image_paths[index % self.n_data], cv2.IMREAD_COLOR)
        # print(image)
        target = self.load_annotation(self.target_lists[index % self.n_data])
        try:
            height, weight, _ = image.shape
        except:
            print(image)

        if self.target_transforms is not None:
            target = self.target_transforms(target)

        if self.preproc is not None:
            image, target = self.preproc(image, target)

        return image, target

    def load_annotation(self, data):
        annotations = np.zeros((0, 5))
        if len(data) == 0:
            return annotations

        for box in data:
            x1 = box[0]
            y1 = box[1]
            x2 = box[2]
            y2 = box[3]

            if (x2 - x1) < 1 or (y2 - y1) < 1:
                continue

            annotation = np.zeros((1, 5))

            annotation[0, 0] = x1
            annotation[0, 1] = y1
            annotation[0, 2] = x2
            annotation[0, 3] = y2

            annotation[0, 4] = box[4]
            annotations = np.append(annotations, annotation, axis=0)

        return annotations
