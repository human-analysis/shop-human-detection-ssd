import os
from glob import glob

import cv2


def resizeImages(image, resX, resY):
    return cv2.resize(image, (resX, resY))


def preprocessing(data_base):
    human = glob(os.path.join(data_base, 'pos', '*.png'))
    nothuman = glob(os.path.join(data_base, 'pos', '*.png'))
    if not os.path.isdir(os.path.join(data_base, 'processed_data')):
        os.mkdir(os.path.join(data_base, 'processed_data'))
