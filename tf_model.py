# ssds part
from libs.modeling.ssds import ssd
from libs.modeling.ssds import ssd_lite
from libs.modeling.ssds import rfb
from libs.modeling.ssds import rfb_lite
from libs.modeling.ssds import fssd_lite
from libs.modeling.ssds import yolo
from libs.utils.config_parse import cfg_from_file, cfg
# from libs.modeling.ssds.fssd import add_extras, FSSD
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import os


class FSSD(nn.Module):
    """FSSD: Feature Fusion Single Shot Multibox Detector
    See: https://arxiv.org/pdf/1712.00960.pdf for more details.

    Args:
        phase: (string) Can be "eval" or "train" or "feature"
        base: base layers for input
        extras: extra layers that feed to multibox loc and conf layers
        head: "multibox head" consists of loc and conf conv layers
        features： include to feature layers to fusion feature and build pyramids
        feature_layer: the feature layers for head to loc and conf
        num_classes: num of classes
    """

    def __init__(self, base, extras, head, features, feature_layer, num_classes):
        super(FSSD, self).__init__()
        self.num_classes = num_classes
        # SSD network
        self.base = nn.ModuleList(base)
        self.extras = nn.ModuleList(extras)
        self.feature_layer = feature_layer[0][0]
        self.transforms = nn.ModuleList(features[0])
        self.pyramids = nn.ModuleList(features[1])
        # print(self.base)
        # Layer learns to scale the l2 normalized features from conv4_3
        self.norm = nn.BatchNorm2d(int(feature_layer[0][1][-1] / 2) * len(self.transforms), affine=True)
        # print(self.extras)

        self.loc = nn.ModuleList(head[0])
        self.conf = nn.ModuleList(head[1])
        # print(self.loc)

        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x, phase='eval'):
        """Applies network layers and ops on input image(s) x.

        Args:
            x: input image or batch of person. Shape: [batch,3,300,300].

        Return:
            Depending on phase:
            test:
                Variable(tensor) of output class label predictions,
                confidence score, and corresponding location predictions for
                each object detected. Shape: [batch,topk,7]

            train:
                list of concat outputs from:
                    1: confidence layers, Shape: [batch*num_priors,num_classes]
                    2: localization layers, Shape: [batch,num_priors*4]

            feature:
                the features maps of the feature extractor
        """
        sources, transformed, pyramids, loc, conf = [list() for _ in range(5)]

        # apply bases layers and cache source layer outputs
        for k in range(len(self.base)):
            x = self.base[k](x)
            if k in self.feature_layer:
                sources.append(x)

        # apply extra layers and cache source layer outputs
        for k, v in enumerate(self.extras):
            x = v(x)
            # TODO: different with lite this one should be change
            if k % 2 == 1:
                sources.append(x)
        assert len(self.transforms) == len(sources)
        # upsize = (sources[0].size()[2], sources[0].size()[3])
        # print(upsize, '=======================')
        #
        for k, v in enumerate(self.transforms):
            up_size = None if k == 0 else (sources[0].size()[2], sources[0].size()[3])
            transformed.append(v(sources[k]))
        up_size = None
        # print(x1, x2, x3)
        # x = torch.cat([x1, x2], 1)
        x = torch.cat(transformed, 1)
        x = self.norm(x)
        for k, v in enumerate(self.pyramids):
            x = v(x)
            pyramids.append(x)

        if phase == 'feature':
            return pyramids

        # apply multibox head to pyramids layers
        for (x, l, c) in zip(pyramids, self.loc, self.conf):
            loc.append(l(x).permute(0, 2, 3, 1).contiguous())
            conf.append(c(x).permute(0, 2, 3, 1).contiguous())
        loc = torch.cat([o.view(o.size(0), -1) for o in loc], 1)
        conf = torch.cat([o.view(o.size(0), -1) for o in conf], 1)

        if phase == 'eval':
            output = (
                loc.view(loc.size(0), -1, 4),  # loc preds
                self.softmax(conf.view(-1, self.num_classes)),  # conf preds
            )
        else:
            output = (
                loc.view(loc.size(0), -1, 4),
                conf.view(conf.size(0), -1, self.num_classes),
            )
        return output
        # return x


class BasicConv(nn.Module):
    def __init__(self, in_planes, out_planes, kernel_size, stride=1, padding=0, dilation=1, groups=1, relu=True,
                 bn=False, bias=True):
        super(BasicConv, self).__init__()
        self.out_channels = out_planes
        self.conv = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding,
                              dilation=dilation, groups=groups, bias=bias)
        self.bn = nn.BatchNorm2d(out_planes, eps=1e-5, momentum=0.01, affine=True) if bn else None
        self.relu = nn.ReLU(inplace=True) if relu else None
        self.up_size = None
        # self.up_sample = nn.Upsample(size=(up_size,up_size),mode='bilinear') if up_size != 0 else None
        #

    def set_upsize(self, upsize):
        self.up_size = upsize

    def forward(self, x):  ###############################################bugs dons have up_size parameter
        x = self.conv(x)
        if self.bn is not None:
            x = self.bn(x)
        if self.relu is not None:
            x = self.relu(x)
        if self.up_size is not None:
            x = F.interpolate(x, size=self.up_size, mode='bilinear', align_corners=False)
            # x = self.up_sample(x)
            # x = nn.Upsample(size=(up_size,up_size),mode='bilinear')(x)
            # x = F.upsample(x, size=up_size, mode='bilinear', align_corners=False)
            # x = F.interpolate()
        return x


class BasicConv_(nn.Module):
    def __init__(self, in_planes, out_planes, kernel_size, stride=1, padding=0, dilation=1, groups=1, relu=True,
                 bn=False, bias=True, up_size=None):
        super(BasicConv_, self).__init__()
        self.out_channels = out_planes
        self.conv = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding,
                              dilation=dilation, groups=groups, bias=bias)
        self.bn = nn.BatchNorm2d(out_planes, eps=1e-5, momentum=0.01, affine=True) if bn else None
        self.relu = nn.ReLU(inplace=True) if relu else None
        self.up_size = 38  # 37 darknet19
        # self.up_sample = nn.Upsample(size=(up_size,up_size),mode='bilinear') if up_size != 0 else None
        #

    def forward(self, x):  ###############################################bugs dons have up_size parameter
        x = self.conv(x)
        if self.bn is not None:
            x = self.bn(x)
        if self.relu is not None:
            x = self.relu(x)
        # if up_size is not None:
        x = F.interpolate(x, size=self.up_size, mode='bilinear', align_corners=False)
        # x = self.up_sample(x)
        # x = nn.Upsample(size=(self.up_size, self.up_size),mode='bilinear')(x)
        # x = F.upsample(x, size=self.up_size, mode='bilinear', align_corners=False)
        # x = F.interpolate()
        return x


def _conv_dw(inp, oup, stride=1, padding=0, expand_ratio=1):
    return nn.Sequential(
        # pw
        nn.Conv2d(inp, oup * expand_ratio, 1, 1, 0, bias=False),
        nn.BatchNorm2d(oup * expand_ratio),
        nn.ReLU6(inplace=True),
        # dw
        nn.Conv2d(oup * expand_ratio, oup * expand_ratio, 3, stride, padding, groups=oup * expand_ratio, bias=False),
        nn.BatchNorm2d(oup * expand_ratio),
        nn.ReLU6(inplace=True),
        # pw-linear
        nn.Conv2d(oup * expand_ratio, oup, 1, 1, 0, bias=False),
        nn.BatchNorm2d(oup),
    )


def add_extras(base, feature_layer, mbox, num_classes, version):
    extra_layers = []
    feature_transform_layers = []
    pyramid_feature_layers = []
    loc_layers = []
    conf_layers = []
    in_channels = None
    feature_transform_channel = int(feature_layer[0][1][-1] / 2)
    for layer, depth in zip(feature_layer[0][0], feature_layer[0][1]):
        if 'lite' in version:
            if layer == 'S':
                extra_layers += [_conv_dw(in_channels, depth, stride=2, padding=1, expand_ratio=1)]
                in_channels = depth
            elif layer == '':
                extra_layers += [_conv_dw(in_channels, depth, stride=1, expand_ratio=1)]
                in_channels = depth
            else:
                in_channels = depth
        else:
            if layer == 'S':
                extra_layers += [
                    nn.Conv2d(in_channels, int(depth / 2), kernel_size=1),
                    nn.Conv2d(int(depth / 2), depth, kernel_size=3, stride=2, padding=1)]
                in_channels = depth
            elif layer == '':
                extra_layers += [
                    nn.Conv2d(in_channels, int(depth / 2), kernel_size=1),
                    nn.Conv2d(int(depth / 2), depth, kernel_size=3)]
                in_channels = depth
            else:
                in_channels = depth
        feature_transform_layers += [BasicConv_(in_channels, feature_transform_channel, kernel_size=1, padding=0)]

    in_channels = len(feature_transform_layers) * feature_transform_channel
    for layer, depth, box in zip(feature_layer[1][0], feature_layer[1][1], mbox):
        if layer == 'S':
            pyramid_feature_layers += [BasicConv(in_channels, depth, kernel_size=3, stride=2, padding=1)]
            in_channels = depth
        elif layer == '':
            pad = (0, 1)[len(pyramid_feature_layers) == 0]
            pyramid_feature_layers += [BasicConv(in_channels, depth, kernel_size=3, stride=1, padding=pad)]
            in_channels = depth
        else:
            AssertionError('Undefined layer')
        loc_layers += [nn.Conv2d(in_channels, box * 4, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(in_channels, box * num_classes, kernel_size=3, padding=1)]
    return base, extra_layers, (feature_transform_layers, pyramid_feature_layers), (loc_layers, conf_layers)


def build_fssd(base, feature_layer, mbox, num_classes):
    base_, extras_, features_, head_ = add_extras(base(), feature_layer, mbox, num_classes, version='fssd')
    return FSSD(base_, extras_, head_, features_, feature_layer, num_classes)


def build_fssd_lite(base, feature_layer, mbox, num_classes):
    base_, extras_, features_, head_ = add_extras(base(), feature_layer, mbox, num_classes, version='fssd_lite')
    return FSSD(base_, extras_, head_, features_, feature_layer, num_classes)


ssds_map = {
    'ssd': ssd.build_ssd,
    'ssd_lite': ssd_lite.build_ssd_lite,
    'rfb': rfb.build_rfb,
    'rfb_lite': rfb_lite.build_rfb_lite,
    'fssd': build_fssd,
    'fssd_lite': fssd_lite.build_fssd_lite,
    'yolo_v2': yolo.build_yolo_v2,
    'yolo_v3': yolo.build_yolo_v3,
}

# nets part
from libs.modeling.nets import vgg
from libs.modeling.nets import resnet
from libs.modeling.nets import mobilenet
from libs.modeling.nets import darknet

networks_map = {
    'vgg16': vgg.vgg16,
    'resnet_18': resnet.resnet_18,
    'resnet_34': resnet.resnet_34,
    'resnet_50': resnet.resnet_50,
    'resnet_101': resnet.resnet_101,
    'mobilenet_v1': mobilenet.mobilenet_v1,
    'mobilenet_v1_075': mobilenet.mobilenet_v1_075,
    'mobilenet_v1_050': mobilenet.mobilenet_v1_050,
    'mobilenet_v1_025': mobilenet.mobilenet_v1_025,
    'mobilenet_v2': mobilenet.mobilenet_v2,
    'mobilenet_v2_075': mobilenet.mobilenet_v2_075,
    'mobilenet_v2_050': mobilenet.mobilenet_v2_050,
    'mobilenet_v2_025': mobilenet.mobilenet_v2_025,
    'darknet_19': darknet.darknet_19,
    'darknet_53': darknet.darknet_53,
}

from libs.layers.functions.prior_box import PriorBox
import torch


def _forward_features_size(model, img_size):
    model.eval()
    x = torch.rand(1, 3, img_size[0], img_size[1])
    with torch.no_grad():
        x = torch.autograd.Variable(x)  # .cuda()
    feature_maps = model(x, phase='feature')
    return [(o.size()[2], o.size()[3]) for o in feature_maps]


def create_model(cfg):
    '''
    '''
    #
    base = networks_map[cfg.NETS]
    number_box = [
        2 * len(aspect_ratios) if isinstance(aspect_ratios[0], int) or isinstance(aspect_ratios[0], float) else len(
            aspect_ratios) for aspect_ratios
        in cfg.ASPECT_RATIOS]
    # base_ = nn.ModuleList(base)
    # y = base_(dummy_input)
    # Export the model
    #
    # print(cfg.SSDS, '=============================')
    model = ssds_map['fssd'](base=base, feature_layer=cfg.FEATURE_LAYER, mbox=number_box, num_classes=cfg.NUM_CLASSES)
    # model = build_fssd(base=base, feature_layer=cfg.FEATURE_LAYER, mbox=number_box, num_classes=cfg.NUM_CLASSES)
    # #
    # dummy_input = torch.randn(1, 3, 300, 300)
    # torch_out = torch.onnx._export(model, dummy_input, "graph.onnx")
    feature_maps = _forward_features_size(model, cfg.IMAGE_SIZE)
    print('==>Feature map size:')
    print(feature_maps)
    # #
    priorbox = PriorBox(image_size=cfg.IMAGE_SIZE, feature_maps=feature_maps, aspect_ratios=cfg.ASPECT_RATIOS,
                        scale=cfg.SIZES, archor_stride=cfg.STEPS, clip=cfg.CLIP)
    # # priors = Variable(priorbox.forward(), volatile=True)
    #
    return model, priorbox


def onnx_generation(saving_model):
    model, priorbox = create_model(cfg.MODEL)
    # create_model(cfg.MODEL)
    with torch.no_grad():
        priors = Variable(priorbox.forward())
    use_gpu = torch.cuda.is_available()

    checkpoint = torch.load(cfg.RESUME_CHECKPOINT, map_location='gpu' if use_gpu else 'cpu')
    model.load_state_dict(checkpoint)

    model.train(False)

    dummy_input = torch.randn(1, 3, cfg.MODEL.IMAGE_SIZE[0], cfg.MODEL.IMAGE_SIZE[1])
    torch.onnx.export(model, dummy_input, saving_model, export_params=True, )
    # # Export the model
    # _input, "graph.onnx", verbose=False, export_params=True)


import cv2
import numpy as np
import tensorflow as tf
import random


# def preproc_for_test(image, insize, mean):
#     interp_methods = [cv2.INTER_LINEAR, cv2.INTER_CUBIC, cv2.INTER_AREA, cv2.INTER_NEAREST, cv2.INTER_LANCZOS4]
#     interp_method = interp_methods[random.randrange(5)]
#     image = cv2.resize(image, (insize[0], insize[1]), interpolation=interp_method)
#     image = image.astype(np.float32)
#     image -= mean
#     return image.transpose(2, 0, 1)

class Preproc():
    def __init__(self, resize, rgb_means):
        self.means = rgb_means
        self.resize = resize

    def __call__(self, image):
        insize = self.resize
        mean = self.means
        interp_methods = [cv2.INTER_LINEAR, cv2.INTER_CUBIC, cv2.INTER_AREA, cv2.INTER_NEAREST, cv2.INTER_LANCZOS4]
        interp_method = interp_methods[random.randrange(5)]
        image = cv2.resize(image, (insize[0], insize[1]), interpolation=interp_method)
        image = image.astype(np.float32)
        image -= mean
        image = image.transpose(2, 0, 1)
        return image


def test():
    COLORS = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
    FONT = cv2.FONT_HERSHEY_SIMPLEX
    # net = cv2.dnn.readNet("graph.pb")
    GRAPH_PB_PATH = 'graph.pb'
    with tf.Session() as sess:
        print("load graph")
        with  tf.gfile.GFile(GRAPH_PB_PATH, 'rb') as f:
            graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')
        graph_nodes = [n for n in graph_def.node]

        # for node in graph_nodes:
        #     print(node.name)
        #     try:
        #         output = tf.get_default_graph().get_tensor_by_name(node.name + ":0")
        #         print(output)
        #     except:
        #         pass
        input = tf.get_default_graph().get_tensor_by_name("0:0")
        out = [None, None]
        out[0] = tf.get_default_graph().get_tensor_by_name("Reshape_88:0")
        out[1] = tf.get_default_graph().get_tensor_by_name("Softmax:0")

        path = os.path.join('./sample/images', '190509_LLQ201_01_00382.jpg')
        # for path in images_list:
        image = cv2.imread(path)
        scale = [image.shape[1], image.shape[0], image.shape[1], image.shape[0]]
        preproc = Preproc(cfg.MODEL.IMAGE_SIZE, cfg.DATASET.PIXEL_MEANS)
        start = time.time()
        image = preproc(image)
        image = np.expand_dims(image, 0)
        locs, confs = sess.run(out, feed_dict={input: image})
        print(time.time() - start, "==================")
        _labels, _coords, _scores = [], [], []
        image1 = cv2.imread(path)
        # print(locs[0].shape, confs.shape)
        # for loc, conf in zip(locs[0], confs):
        #     if conf[0] > 0.3:
        #         # print(111111111)
        #         _labels.append(1)
        #         _coords.append(loc * scale)
        #         _scores.append(conf[0])
        # print(len(loc[0]), len(confs))
        # for labels, scores, coords in zip(_labels, _scores, _coords):
        #     # print(coords, scores, labels)
        #     cv2.rectangle(image1, (int(coords[0]), int(coords[1])), (int(coords[2]), int(coords[3])), COLORS[labels % 3],
        #                   2)
        #     cv2.putText(image1, '{label}: {score:.3f}'.format(label='person', score=scores),
        #                 (int(coords[0]), int(coords[1])), FONT, 0.5, COLORS[0], 2)
        #
        # # 7. visualize result
        # # if args.display is True:
        # cv2.imshow('result', image1)
        # cv2.waitKey()


import time

if __name__ == "__main__":
    cfg_from_file('./cfgs/fssd_vgg16_train_smartshop.yml')
    # onnx_generation('fssd_lite_mobilenetv1.onnx')
    # test()
    # net = cv2.dnn.readNetFromONNX('mobilenetv1_fssd.onnx')
    # graph = onnx.load('graph.onnx')
    # graph = shape_inference.infer_shapes(graph)
    # print(graph)
    # test("mobilenetv1_fssd.pb")
    # graph = onnx.load('graph.onnx')
    # graph = shape_inference.infer_shapes(graph)
    # print(graph)
    # net = cv2.dnn.readNetFromONNX('graph.onnx')
    # net = cv2.dnn.readTensorFromONNX('graph.onnx')
    # net = cv2.dnn.readNetFromONNX('mobilenetv1_fssd.onnx')
    model, priorbox = create_model(cfg.MODEL)
    x = torch.Tensor(np.random.randint(0, 254, (1, 3, 300, 300)).astype(np.float32))
    start = time.time()
    y = model(x)
    print(time.time() - start)
    # # create_model(cfg.MODEL)
    # with torch.no_grad():
    #     priors = Variable(priorbox.forward())
    # use_gpu = torch.cuda.is_available()
    #
    # checkpoint = torch.load(cfg.RESUME_CHECKPOINT, map_location='gpu' if use_gpu else 'cpu')
    # model.load_state_dict(checkpoint)
    #
    # model.eval()
    # input_var = Variable(torch.randn(1, 3, 300, 300))
    # output_var = model(input_var)
    # print(type(output_var))
    #
    # pytorch2caffe(input_var, output_var,
    #               os.path.join(".", 'test.prototxt'),
    #               os.path.join(".", 'test.caffemodel'))
    import time
    import numpy as np

    img = np.random.random((1, 3, 300, 300)).astype(np.float32)
    # iters = 20
    #
    # import onnxruntime as rt
    #
    # sess = rt.InferenceSession("optimized_mobilenetv1_fssd.onnx")
    #
    # input_name = sess.get_inputs()[0].name
    # input0 = sess.get_outputs()[0].name
    # input1 = sess.get_outputs()[0].name
    #
    # _start_time = time.time()
    # for i in range(iters):
    #     sess.run([input0, input1], {input_name: x.astype(np.float32)})[0]
    #
    # print(time.time() - _start_time)
    # from onnx_tf.backend import prepare
    # import tensorflow as tf
    # from tensorflow.python.client import timeline
    # model = onnx.load("optimized_graph.onnx")
    # tf_rep = prepare(model, strict=True)
    #
    # print("Running")  # this should run fast
    # run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
    # run_metadata = tf.RunMetadata()
    #
    # config = tf.ConfigProto()
    # config.gpu_options.allow_growth = True
    # sess = tf.Session(config=config, graph=None)
    #
    # tf.import_graph_def(tf_rep.graph.as_graph_def(), name="")
    # graph = tf.get_default_graph()
    # for op in tf.get_default_graph().get_operations():
    #     print(op.name)
    #
    # start = time.time()
    # for i in range(10):
    #     sess.run("Softmax:0", feed_dict={"transpose:0": img})  # tf_rep.run(img)
    #     # print(output)
    #
    # end = time.time()
    # print("time elapsed:")
    # print((end - start) / 10)
